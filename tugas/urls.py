from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='index'),
    path('home/', views.index, name='index'),
    path('projects/', views.projects, name='projects'),
    path('extra/', views.extra, name='extra'),
	path('form/', views.matkulForm, name='form'),
	path('list/', views.matkulList, name='list'),
	path('form/save/', views.save_matkul, name='save'),
	path('form/delete/', views.delete_matkul, name='delete'),
]