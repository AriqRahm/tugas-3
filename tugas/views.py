from django.shortcuts import render
from .forms import CreateMatkulForm, DeleteMatkulForm
from .models import MataKuliah

# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})
def index(request):
    return render(request,'pages/index.html',{})
def projects(request):
    return render(request,'pages/projects.html',{})
def extra(request):
    return render(request,'pages/extra.html',{})	
def matkulForm(request):
	response = {'create_matkul_form': CreateMatkulForm(), 'delete_matkul_form': DeleteMatkulForm()}
	return render(request,'pages/matkul_form.html',response)
def matkulList(request):
	matkuls = MataKuliah.objects.all().values()
	response = {'matkuls':matkuls}
	return render(request,'pages/matkul_view.html',{})
def save_matkul(request):
	response = {'save_status':''}
	if(request.method == 'POST'):
		form = CreateMatkulForm(request.POST)
		if (form.is_valid()):
			subject = form.cleaned_data['subject']
			lecturer = form.cleaned_data['lecturer']
			sks = form.cleaned_data['sks']
			year = form.cleaned_data['year']
			room = form.cleaned_data['room']
			desc = form.cleaned_data['desc']

			matkul = MataKuliah(
                subject = subject,
                lecturer = lecturer,
                sks = sks,
                year = year,
                room = room,
                desc = desc
            )

			matkul.save()

			response['save_status'] = 'Subject saved succesfully'
		else:
			response['save_status'] = 'Failed to add subject'
	else:
		response['save_status'] = 'Something happened, please try again later'
	return render(request, 'save_matkul.html', response)
def delete_matkul(request):
	response = {'delete_status':''}
	if(request.method == 'POST'):
		form = DeleteMatkulForm(request.POST)
		if (form.is_valid()):
			matkul = form.cleaned_data['choice']
			matkul.delete()

			response['delete_status'] = 'Subject deleted succesfully'
		else:
			response['delete_status'] = 'Failed to delete subject'
	else:
		response['delete_status'] = 'Something happened, please try again later'
	return render(request, 'delete_matkul.html', response)







	
