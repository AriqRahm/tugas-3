from django import forms
from .models import MataKuliah

class CreateMatkulForm(forms.Form):

    subject_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Please enter subject name...'
    }

    lecturer_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Please enter lecturer name...'
    }
	
    sks_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Please enter the ammount of SKS...'
    }
	
    desc_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Please enter a description...'
    }
	
    room_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Please enter the class...'
    }
	
    subject = forms.CharField(label='Subject', max_length=50,  required=True, widget=forms.TextInput(attrs=subject_attrs))
    lecturer = forms.CharField(label='Lecturer', max_length=50,  required=True, widget=forms.TextInput(attrs=lecturer_attrs))
    sks = forms.CharField(label='SKS', max_length=2,  required=True, widget=forms.TextInput(attrs=sks_attrs))
	
    year_attrs = {
        'class': 'custom-select'
    }
    choices = [
        ('2019/2020', '2019/2020'),
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
   ]
	
    room = forms.CharField(label='Class', max_length=1,  required=True, widget=forms.TextInput(attrs=room_attrs))
    desc = forms.CharField(label='Description', max_length=50,  required=True, widget=forms.TextInput(attrs=desc_attrs))
    year = forms.ChoiceField(label='Academic Year', choices = choices, required=True, widget=forms.Select(attrs=year_attrs))
	
class CustomModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{} {}, Tahun akademik {} ({})".format(obj.subject, obj.room, obj.year, obj.lecturer)

class DeleteMatkulForm(forms.Form):
    choice_attrs = {
        'class':'custom-select'
    }

    matkuls = MataKuliah.objects.all()
    choice = CustomModelChoiceField(label="Matkul", required=True, widget=forms.Select(choice_attrs), queryset=matkuls, to_field_name="subject")

